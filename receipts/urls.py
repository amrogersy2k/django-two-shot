from django.urls import path
from receipts.views import (
    Receipt_list,
    ExpenseCategory_list,
    Account_list,
    create_receipt,
    create_category,
    create_account,
)

urlpatterns = [
    path("", Receipt_list, name="home"),
    path('create/', create_receipt, name='create_receipt'),
    path('categories/', ExpenseCategory_list, name='category_list'),
    path('categories/create/', create_category, name='create_category'),
    path('accounts/', Account_list, name='account_list'),
    path('accounts/create/', create_account, name='create_account'),
]
